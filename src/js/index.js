$(document).ready(function () {
$('.our-work__slider').slick({
        autoplay: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        dots: true,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2       
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1       
      }
    }    
  ]
});
})